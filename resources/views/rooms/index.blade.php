@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <div class="panel panel-default">
                    <room-list></room-list>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Users</div>

                    <div class="panel-body">
                        @foreach($users as $user)
                            <p><a href="{{route('rooms.dialog',['user'=>$user])}}">
                                    {{$user->name}}
                                </a></p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
