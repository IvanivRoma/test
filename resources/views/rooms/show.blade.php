@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>
                            @if($room->is_dialog)
                                Dialog
                            @else
                                Room
                            @endif
                        </strong>
                        {{ $room->name }}
                    </div>

                    <div class="panel-body">
                        <room-messages :messages="messages" :auth-user="{{ Auth::user() }}"></room-messages>
                    </div>
                    <div class="panel-footer">
                        <input type="hidden" id="roomId" value="{{ $room->id }}">
                        <room-form
                                v-on:messagesent="addMessage"
                                :user="{{ Auth::user() }}"
                                :room-id="{{ $room->id }}"
                        ></room-form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <room-settings :messages="messages" @selectdate="setMessages"
                               :users="users"
                               :room-id="{{ $room->id }}">
                </room-settings>
            </div>
        </div>
    </div>
@endsection