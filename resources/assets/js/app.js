/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import axios from 'axios';
import Buefy from 'buefy';
import 'buefy/lib/buefy.css';


require('./bootstrap');

window.Vue = require('vue');
Vue.use(Buefy);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('room-messages', require('./components/Chat/MessagesList.vue'));
Vue.component('room-form', require('./components/Chat/Form.vue'));
Vue.component('room-list', require('./components/Rooms/RoomList.vue'));
Vue.component('room-settings', require('./components/Chat/Settings.vue'));


let app = new Vue({
    el: '#app',

    data: {
        messages: [],
        users: [],
        roomId: null
    },

    mounted() {
        let room = document.getElementById('roomId');
        if (room) {
            this.roomId = document.getElementById('roomId').value;
            this.fetchMessages();
            window.Echo.private('room.' + this.roomId)
                .listen('MessageSent', (e) => {
                    e.message.user = e.user;
                    e.message.attachments = e.attachments;
                    this.messages.push(e.message);
                });
            window.Echo.private('room.' + this.roomId)
                .listen('MessageUpdate', (e) => {
                    let objIndex = this.messages.findIndex((obj => obj.id == e.message.id));
                    this.messages[objIndex].message = e.message.message
                });
            window.Echo.private('room.' + this.roomId)
                .listen('MessageDelete', (e) => {
                    let objIndex = this.messages.findIndex((obj => obj.id == e.message.id));
                    this.messages.splice(objIndex, 1)
                });
        }
    },

    methods: {
        fetchMessages() {
            axios.get(this.roomId + '/messages').then(response => {
                this.messages = response.data.messages;
                this.users = response.data.users;
            });
        },
        setMessages(messages) {
            this.messages = messages
        },

        addMessage(message) {
            axios.post('/messages', message).then(response => {
                console.log(response.data);
                this.messages.push(response.data);
            });
        }
    }
});
