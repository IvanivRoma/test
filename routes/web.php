<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('rooms.index');
});

Auth::routes();

Route::group(['middleware' =>'auth'], function () {
    Route::get('chat', 'RoomsController@index')->name('rooms.index');
    Route::get('rooms', 'RoomsController@all')->name('rooms.all');
    Route::post('rooms', 'RoomsController@store')->name('rooms.store');
    Route::post('messages', 'MessagesController@store')->name('messages.store');
    Route::put('messages/{message}', 'MessagesController@update')->name('messages.update');
    Route::delete('messages/{message}', 'MessagesController@destroy')->name('messages.delete');

    Route::get('dialog/{user}', 'RoomsController@showDialog')->name('rooms.dialog');
    Route::group(['prefix' => 'room/{room}'], function () {
        Route::get('/', 'RoomsController@show')->name('rooms.show');
        Route::post('/users', 'RoomsController@addUser');
        Route::get('fetch-users', 'RoomsController@fetchUsers')->name('users.fetch');
        Route::get('/messages/', 'MessagesController@index');
    });
});
