<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Attachment extends Model
{
    protected $fillable = ['file_name', 'file_path', 'message_id',];

    /**
     * Get message that owns attachment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo('App/Message');
    }

    /**
     * Upload and save Attachment
     *
     * @param Builder $query
     * @param $attachments
     * @param $message
     */
    public function scopeStore(Builder $query, $attachments, $message)
    {
        if (count($attachments))
            foreach ($attachments as $attachment) {
                list($mime, $file) = explode(';', $attachment);
                list(, $file) = explode(',', $file);
                $file = base64_decode($file);
                $mime = explode(':', $mime)[1];
                $ext = explode('/', $mime)[1];
                $name = mt_rand() . time();
                $savePath = 'uploads/images/' . $name . '.' . $ext;
                file_put_contents(public_path() . '/' . $savePath, $file);
                Attachment::create([
                    'file_name' => $name,
                    'file_path' => request()->getHttpHost() . '/' . $savePath,
                    'message_id' => $message['id']]);
            }
    }
}
