<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Room extends Model
{

    protected $fillable = ['name', 'is_public', 'is_dialog',];

    /**
     * Users that belongs to room
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * Get massages owns to room
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * Return list of available rooms
     *
     * @param Builder $q
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function scopeAvailableRooms(Builder $q)
    {
        return $q->where('is_public', '=', 1)
            ->orWhereHas('users', function ($q) {
                return $q->where('users.id', '=', Auth::user()->id);
            })
            ->get();
    }

    /**
     * Return or create dialog with user
     *
     * @param Builder $q
     * @param User $user
     * @return mixed
     */
    public function scopeHasDialog(Builder $q, User $user)
    {
        $room = $q->where('is_dialog', '=', 1)
            ->join('room_user', function ($join) {
                $join->on('rooms.id', '=', 'room_user.room_id')
                    ->where('room_user.user_id', '=', Auth::user()->id);
            })
            ->join('room_user as dialog', function ($join) use ($user) {
                $join->on('rooms.id', '=', 'dialog.room_id')
                    ->where('dialog.user_id', '=', $user->id);
            })->get()->first();
        if (empty($room->room_id)) {
            $room = Room::create(['name' => $user->name . '-' . Auth::user()->name, 'is_dialog' => 1]);
            $room->users()->attach([Auth::user()->id, $user->id]);
            return $room->id;
        }
        return $room->room_id;
    }
}
