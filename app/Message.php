<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    protected $fillable = ['message', 'user_id', 'room_id'];

    protected $appends = ['self'];

    /**
     * Get attachments for message
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    /**
     * Get user that owns message
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Check if message belongs to user
     *
     * @return bool
     */
    public function getSelfAttribute()
    {
        return $this->user_id == Auth::user()->id;
    }
}
