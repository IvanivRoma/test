<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Events\MessageDelete;
use App\Events\MessageSent;
use App\Events\MessageUpdate;
use App\Message;
use App\Room;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Room $room
     * @return \Illuminate\Http\Response
     */
    public function index(Room $room)
    {
        $message = Message::with('user')->where('room_id', $room->id);
        if ($date = \request('created_at')) {
            $date = date_format(new \DateTime($date), 'Y-m-d');
            $message->whereDate('created_at', $date);
        }
        return ['messages' => $message->with('attachments')->get(), 'users' => $room->users];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $message = $user->messages()->create([
            'message' => $request->input('message'),
            'room_id' => $request->input('room_id')
        ]);
        Attachment::store($request->input('attachments'), $message);
        $att = Attachment::where('message_id', '=', $message->id)->get();
        broadcast(new MessageSent($user, $message, $att))->toOthers();
        $message->attachments = $att;
        $message->user = $user;
        return $message;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Message $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        $message->update([
            'message' => $request->input('message'),
            'room_id' => $request->input('room_id'),
            'user_id' => $request->input('user_id')
        ]);
        $message = Message::find($message->id);
        broadcast(new MessageUpdate($message->user, $message))->toOthers();
        return $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        $old_message = $message;

        broadcast(new MessageDelete($old_message->user, $old_message))->toOthers();
        $message->delete();
        return ['status' => 'deleted'];
    }
}
