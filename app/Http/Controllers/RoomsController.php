<?php

namespace App\Http\Controllers;

use App\Message;
use App\Room;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class RoomsController extends Controller
{
    /**
     * RoomsController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $room = Room::find($request->room);
            if (!$room->contains(Auth::user()->id) && !$room->is_public)
                abort(403);
            return $next($request);
        })->only('addUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', '<>', Auth::user()->id)->get();
        return view('rooms.index', compact(['users']));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $rooms = Room::availableRooms();
        return $rooms;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $room = Room::create([
            'name' => $request->input('name'),
            'is_public' => $request->input('is_public')
        ]);
        $room->users()->attach(Auth::user()->id);
        return $room;
    }

    /**
     * Show selected chat
     *
     * @param Room $room
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(Room $room)
    {
        if ($room->is_public || $room->users->contains(Auth::user()->id)) {
            $room->users()->sync(Auth::user()->id, false);
            return view('rooms.show', compact('room'));
        }
        return redirect()->back();
    }

    /**
     * Show dialog with selected user
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function showDialog(User $user)
    {
        return redirect()->route('rooms.show', ['room' => Room::hasDialog($user)]);
    }

    /**
     * Add user to chat
     *
     * @param Request $request
     * @param Room $room
     * @return array|bool
     */
    public function addUser(Request $request, Room $room)
    {
        if ($room->users->contains($request->id))
            return false;
        else
            return $room->users()->sync($request->id, false);
    }

    /**
     * Fetch users from room
     *
     * @param Request $request
     * @param Room $room
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function fetchUsers(Request $request, Room $room)
    {
        $room_users = $room->users->pluck('id')->toArray();
        if ($request->filled('name')) {
            return User::query()->where('name', 'like', '%' . $request->name . '%')
                ->whereNotIn('id', $room_users)->get();
        }
    }
}
