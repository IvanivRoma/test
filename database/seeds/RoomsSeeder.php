<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=1;$i<20;$i++) {
            DB::table('rooms')->insert([
                'name' => $faker->jobTitle,
                'is_public' => 1,
                'is_dialog' => 0,
            ]);
        }
    }
}
